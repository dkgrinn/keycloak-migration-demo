package dk.grinn.keycloak.migrate;

/*-
 * #%L
 * Keycloak : Migration : Demo
 * %%
 * Copyright (C) 2019 Jonas Grann & Kim Jersin
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */
import dk.grinn.keycloak.migration.annotation.Migration;
import dk.grinn.keycloak.migration.core.JavaMigration;
import java.util.Map;
import javax.inject.Inject;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.RealmRepresentation;

/**
 *
 * @author @author Kim Jersin &lt;kim@jersin.dk&gt;
 */
@Migration
public class V01__Initial implements JavaMigration {

    @Inject
    private RealmResource resource;
    
    @Override
    public void migrate() throws Exception {
        RealmRepresentation rep = resource.toRepresentation();
        
        // Email
        rep.setSmtpServer(Map.of(
                "host", "smtp.example.com",
                //                "port", "25",
                "fromDisplayName", "Keycloak Migration Demo",
                "from", "no-reply@grinn.dk",
                "ssl", "true",
                "starttls", "true",
                "auth", "true",
                "user", "smtp username",
                "password", "test1234"
        ));

        // ... and do the update
        resource.update(rep);
    }
}
