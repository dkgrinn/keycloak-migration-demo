# keycloak-migration-demo

Example project of how to organize a keycloak migration project for developing
migrations and how to create a distribution zip file for applying the migrations
in a production environment using the `gkcadm-command-line` package as baseline.

## Development

**Setup the keycloak server**  
*## insert link to the keycloak migration project readme file ##*

**Config**  
Edit `gkcadm.conf`

**Clean**  
```shell
mvn install exec:java@clean
```

**Migrate**  
```shell
mvn install exec:java@migrate
```

**Info**  
```shell
mvn install exec:java@info
```

**All the above in one go**  
```shell
mvn install exec:java@recreate
```

**Using a realm from the maven command line**  
*Overrides the `gkcadm.conf` specified realm*
```shell
mvn -Dgkcadm.realms=Hello42 install exec:java@recreate
```

*NOTE*: A proper maven plugin is in the pipeline.

## Distribution

**Create a distribution**  
```shell
mvn -Pdistro install
```

This creates an additional artifact of type `zip` (`keycloak-migrate-demo-<version>.zip`).  
It contains all the necessary dependencies **AND** the demo migration artifact itself.

**Use the distribution**  
```shell
unzip keycloak-migrate-demo-<version>.zip
keycloak-migrate-demo-<version>.zip/gkcadm --configFiles=<config_filename> migrate info
```

Individual configuration arguments can be overwritten from environment variables
( *## insert link to keycloak migration configuration readme ##* ).

Using an environment variable to override the realm:  
```shell
GKCADM_REALMS=Hello42 keycloak-migrate-demo-<version>.zip/gkcadm --configFiles=<config_filename> migrate info
```
